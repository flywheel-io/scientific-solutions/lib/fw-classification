# Operators

## Match Operators

Match operators are used to match specific criteria in the input dictionary ("i_dict").

### Binary Operators

Binary operators compare the value of a specific field from the input dictionary to a
pre-defined value.  They take the form of:

```yaml
- key: <field>
  <op>: <value>
  <config_option>: <value>
```

Where `<field>` is the input dictionary field to look at, and `<value>` is specific to
each operator.

!!! note
    `<value>` can resolve additional fields for the input dictionary by using the
    `"'resolve' : true"` config option

{{detailed_binary_match}}

### Unary Operators

Unary operators are used to group other expressions, and take the form of:

```yaml
- <op>
    - <expr>
    # ... More expressions 
```

Where `<expr>` is either another [Unary Operator](#unary-operators), or a [Binary
Operator](#binary-operators).

{{detailed_unary_match}}

#### Unary operator examples

```yaml
- match_type: "any" 
  match:
    - and:
        - key: file.info.header.dicom.PatientName
          is: "anonymized"
        - key: file.info.header.dicom.PatientAge
          is: "anonymized"
    - key: file.info.anonymized
      is: true
```

> Match if the key `file.info.anonymized` is true, or if both
> `file.info.header.dicom.PatientName` and `file.info.header.dicom.PatientAge`
> are `anonymized`

```yaml
- match_type: "all" 
  match:
    - or:
        - key: file.info.header.dicom.ImageType
          contains: "DERIVED"
        - key: file.info.header.dicom.ImageType
          contains: "SECONDARY"
    - key: file.info.header.dicom.ImageOrientationPatient
      exists: true
```

> Match if the key `file.info.header.dicom.ImageType` contains "DERIVED" or
> SECONDARY", and if the key `file.info.header.dicom.ImageOrientationPatient` exists.

```yaml
- match_type: "all" 
  match:
    - not:
      - or:
          - key: file.info.header.dicom.ImageType
            contains: "DERIVED"
          - key: file.info.header.dicom.ImageType
            contains: "SECONDARY"
    - key: file.info.header.dicom.ImageOrientationPatient
      exists: true
```

> Match if the key `file.info.header.dicom.ImageType` doesn't contain either "DERIVED"
> or "SECONDARY", and if the key `file.info.header.dicom.ImageOrientationPatient` exists.

## Action operators

Action operators are used to apply changes to the input dictionary when its rule matches
the input.  They take a form similar to [Binary Match Operators](#binary-operators):

```yaml
- key: <field>
  <op>: <value>
```

Where `<field>` is the dictionary field to apply the action to, and `<value>` is
specific to each operator.

{{action_details}}

Ex.:

```yaml
- match:
    - key: file.type
      is: 'dicom'
    - key: file.info.header.dicom.Modality:
      is: 'MR'
  action:
    - key: file.classification
      set: 'MR'
```
