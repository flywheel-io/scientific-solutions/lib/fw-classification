from unittest.mock import MagicMock

from fw_classification.classify import Profile, run_classification


def test_classify(mocker, tmp_path):
    p_mock = mocker.patch("fw_classification.classify.Profile", spec=Profile)
    p_mock.return_value.evaluate.return_value = (
        MagicMock(),
        MagicMock(),
    )
    profile = tmp_path / "profile.yml"
    i_dict = MagicMock()
    run_classification(profile, i_dict)
    p_mock.assert_called_once_with(profile)
    p_mock.return_value.evaluate.assert_called_once_with(i_dict)


def test_classify_from_profile():
    profile = MagicMock()
    profile.evaluate.return_value = (MagicMock(), MagicMock())
    i_dict = MagicMock()
    run_classification(profile, i_dict)
    profile.evaluate.assert_called_once_with(i_dict)
