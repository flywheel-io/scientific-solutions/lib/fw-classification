"""Mkdocs plugin for FLywheel CLI."""

from functools import partial
from pathlib import Path

import mkdocs.plugins
import mkdocs.utils
from jinja2 import FileSystemLoader, Template

from fw_classification.classify.expressions import (
    ActionExpression,
    MatchExpression,
    expression_map,
)

DIR = Path(__file__).parent


class FWPlugin(mkdocs.plugins.BasePlugin):
    """Mkdocs plugin to generate and template CLI docs."""

    # pylint: disable=unused-argument

    def __init__(self, *args, **kwargs):
        """Initialize the plugin."""
        super().__init__(*args, **kwargs)
        self.usages = {}
        self.template_loader = FileSystemLoader(f"{DIR}/templates")

    def on_serve(self, server, config, builder):  # pylint: disable=no-self-use
        """Watch CLI source dir to enable livereload on code changes."""
        # TODO can remove this once new mkdocs released where it can be
        # configured from config file
        server.watch(f"{DIR.parent}/fw_classification", builder)
        return server

    def on_page_markdown(self, markdown, page, config, files) -> str:
        """Template the loaded markdown using Jinja."""
        template = Template(markdown)
        template.environment.loader = self.template_loader
        base_url = mkdocs.utils.get_relative_url(".", page.url)
        asciinema_ = partial(asciinema, base_url=base_url)
        simple_doc, details = gen_expressions()
        return template.render(
            asciinema=asciinema_,
            simple_binary_match=render_simple(simple_doc["binary_match"]),
            simple_binary_action=render_simple(simple_doc["binary_action"]),
            simple_unary_match=render_simple(simple_doc["unary"]),
            detailed_binary_match=render_detailed(details["binary_match"], depth=4),
            detailed_unary_match=render_detailed(details["unary"], depth=4),
            action_details=render_detailed(details["binary_action"]),
        )


def asciinema(path, poster="npt:0:0.1", theme="fw", rows="25", base_url=""):
    """Return ascciinema HTML tag."""
    return (
        f'<asciinema-player src="{base_url}/assets/asciinema/{path}" '
        f'poster="{poster}" rows="{rows}" theme="{theme}" />'
    )


def gen_expressions():
    """Generate expressions dict."""
    simple_docs = {}
    details = {}
    for _, (typ_, cls) in expression_map.items():
        key = f"{typ_}"
        if issubclass(cls, MatchExpression):
            key = f"{typ_}_match"
        elif issubclass(cls, ActionExpression):
            key = f"{typ_}_action"
        doc = cls.__doc__
        simple_doc = doc.split("\n")[0]
        simple_docs.setdefault(key, {}).update({cls.op: simple_doc})
        details.setdefault(key, {}).update({cls.op: doc})
    return simple_docs, details


def render_simple(exprs):
    """Render oneline expression."""
    to_render = ""
    for expr, doc in exprs.items():
        to_render += f"* `{expr}`: {doc}\n"
    return to_render


def render_detailed(exprs, depth=3):
    """Render expression with options."""
    to_render = ""
    for expr, doc in exprs.items():
        to_render += f"{'#'*depth} {expr}\n\n{doc}\n"
    return to_render
