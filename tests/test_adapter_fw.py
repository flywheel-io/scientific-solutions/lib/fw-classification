from unittest.mock import MagicMock

import pytest
from dotty_dict import dotty
from fw_client import FWClient

from fw_classification.adapters import fw
from fw_classification.adapters.utils import FileInput


def test_get_hierarchy_successful(core):
    acq = {"parents": {"session": 1, "subject": 1, "project": 1}}
    core.add_response("/api/acquisitions/1", acq)
    for c_type in ["projects", "subjects", "sessions"]:
        core.add_response(f"/api/{c_type}/1", {"label": f"{c_type}-test", "test": 1})
    client = FWClient(url=core.url, api_key="asdf")
    file_input = MagicMock()
    file_input.hierarchy.type = "acquisition"
    file_input.hierarchy.id = 1
    i_dict = {}
    fw.get_hierarchy(i_dict, client, file_input)

    assert i_dict == {
        "acquisition": {},
        "session": {"label": "sessions-test"},
        "subject": {"label": "subjects-test"},
        "project": {"label": "projects-test"},
    }


def test_fw_adapter_init_have_core(empty_file, gear_context):
    f = fw.FWAdapter(empty_file, gear_context, version="1.0.0")
    assert f.gtk == gear_context
    assert f.fw.config.api_key == "test"
    assert f.fw.config.client_name == "fw-classification"
    assert f.fw.config.client_version == "1.0.0"


def test_fw_adapter_init_dont_have_core(empty_file, gear_context):
    fw.HAVE_CORE = False
    f = fw.FWAdapter(empty_file, gear_context, version="1.0.0")
    assert f.gtk == gear_context
    assert f.fw is None
    fw.HAVE_CORE = True


def test_fw_adapter_init_no_api_key(empty_file):
    gear_context = MagicMock()
    gear_context.config_json = {"inputs": {"api-key": {}}}
    f = fw.FWAdapter(empty_file, gear_context, version="1.0.0")
    assert f.gtk == gear_context
    assert f.fw is None


def test_fw_adapter_init_raises_no_gtk(empty_file):
    fw.HAVE_GTK = False
    with pytest.raises(ValueError):
        _ = fw.FWAdapter(empty_file, MagicMock())

    fw.HAVE_GTK = True
    with pytest.raises(ValueError):
        _ = fw.FWAdapter(empty_file, None)


def test_fw_adapter_preprocess(empty_file, mocker, gear_context):
    get_hierarchy = mocker.patch("fw_classification.adapters.fw.get_hierarchy")
    f = fw.FWAdapter(empty_file, gear_context, version="1.0.0")
    res = f.preprocess()
    out_f = FileInput(**empty_file).object.dict()
    exp = {"file": {"name": "", **out_f}}
    assert res == exp
    get_hierarchy.assert_called_once_with(exp, f.fw, f.file)


def test_fw_adapter_postprocess(empty_file, gear_context):
    out = dotty(
        {"file": {"name": "", **empty_file["object"]}, "acquisition": {"label": "test"}}
    )
    f = fw.FWAdapter(empty_file, gear_context, version="1.0.0")
    res = f.postprocess(True, out)
    assert res
    gear_context.metadata.update_container.assert_called_once_with(
        "acquisition", label="test"
    )
    gear_context.metadata.update_file.assert_called_once()
    for key in fw.META_EXCLUDE:
        assert key not in gear_context.metadata.update_file.call_args.kwargs
