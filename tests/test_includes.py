import subprocess
from pathlib import Path
from unittest.mock import MagicMock

import pytest

from fw_classification.classify.includes import get_git_profile, get_profile


@pytest.fixture(scope="function")
def set_env(monkeypatch, tmp_path):
    monkeypatch.setenv("FW_CLASSIFICATION_TEMP_DIR", tmp_path)
    return tmp_path


def test_get_profile_git(mocker):
    # https://www.youtube.com/watch?v=HIrKSqb4H4A
    get_got = mocker.patch("fw_classification.classify.includes.get_git_profile")
    out = get_profile(MagicMock(), "test$test")
    get_got.assert_called_once_with("test$test")
    assert out == get_got.return_value


def test_get_profile_relative(tmp_path):
    search1 = tmp_path / "test_folder"
    search2 = tmp_path / "test_folder2"
    search1.mkdir()
    search2.mkdir()
    (search2 / "test.yaml").touch()
    search_dirs = [search1, search2]
    out = get_profile(search_dirs, "test.yaml")
    assert out == search2 / "test.yaml"


def test_get_profile_absolute():
    out = get_profile(MagicMock(), "/test")
    assert out == Path("/test")


def test_get_git_profile_has_git(mocker, caplog, set_env):
    sp = mocker.patch("fw_classification.classify.includes.subprocess")

    shutil = mocker.patch("fw_classification.classify.includes.shutil")
    shutil.which.return_value = "test"
    caplog.set_level(0)
    out = get_git_profile("test_repo$test_path")
    shutil.which.assert_called_once_with("git")
    assert out.name == "test_path"
    sp.run.assert_called_once_with(
        ["git", "clone", "--depth", "1", "test_repo", str(set_env)], check=True
    )
    assert caplog.record_tuples[0][2] == "Found git at test"


def test_get_git_profile_no_git(mocker):
    shutil = mocker.patch("fw_classification.classify.includes.shutil")
    shutil.which.return_value = None
    with pytest.raises(RuntimeError):
        _ = get_git_profile("test$test")


def test_git_profile_git_call_fails(  # pylint: disable=unused-argument
    mocker, caplog, set_env
):
    caplog.set_level(20)
    sp = mocker.patch("fw_classification.classify.includes.subprocess.run")
    sp.side_effect = subprocess.CalledProcessError(0, MagicMock())
    shutil = mocker.patch("fw_classification.classify.includes.shutil")
    shutil.which.return_value = "test"
    with pytest.raises(RuntimeError):
        _ = get_git_profile("test$test")
    assert caplog.record_tuples[0][2] == "Could not clone git dependency test"
