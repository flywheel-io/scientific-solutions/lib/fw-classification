from fw_classification.classify import *  # pylint: disable=unused-wildcard-import,wildcard-import
from fw_classification.classify.expressions import *  # pylint: disable=unused-wildcard-import,wildcard-import


def test_all_subclasses():
    x = expression_map.copy()
    subcls = [
        "and",
        "or",
        "not",
        # Match types
        "contains",
        "exists",
        "is_numeric",
        "is_empty",
        "is",
        "in",
        "startswith",
        "regex",
        "greater_than",
        "less_than",
        # Action types
        "add",
        "set",
    ]
    for sub in subcls:
        try:
            x.pop(sub)
        except KeyError:
            assert False
    assert len(x) == 0
