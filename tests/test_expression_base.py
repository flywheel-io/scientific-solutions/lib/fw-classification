from unittest.mock import MagicMock

import pytest

from fw_classification.classify.expressions.expression import *  # pylint: disable=unused-wildcard-import,wildcard-import
from fw_classification.classify.utils import ProfileError


@pytest.mark.parametrize(
    "in_dict, out",
    [
        # Simple Binary
        ({"key": "field", "is": "val"}, Is("field", "val")),
        # Simple unary
        ({"not": [{"key": "field", "is": "val"}]}, Not([Is("field", "val")])),
        # Nested
        (
            {
                "not": [
                    {
                        "or": [
                            {"key": "field", "is": "val"},
                            {
                                "and": [
                                    {"key": "field", "regex": "^test"},
                                    {"key": "field2", "is": "test1"},
                                ]
                            },
                        ]
                    }
                ]
            },
            Not(
                [
                    Or(
                        [
                            Is("field", "val"),
                            And([Regex("field", "^test"), Is("field2", "test1")]),
                        ]
                    )
                ]
            ),
        ),
    ],
)
def test_expression_from_dict(in_dict, out):
    expr, err = Expression.from_dict(in_dict)
    assert out == expr
    assert not err
    assert hash(out) == hash(expr)
    assert expr.to_dict() == in_dict


@pytest.mark.parametrize(
    "in_dict",
    [{"or": "val", "field2": "val2", "a": "b"}, {"field1": "field", "is": "value"}],
)
def test_expression_from_dict_errs_simple(in_dict):
    out, e = Expression.from_dict(in_dict)
    assert out is None
    assert len(e) >= 1
    assert isinstance(e[0], ProfileError)
    assert e[0].component == "expression"
    assert e[0].raw == in_dict
    assert e[0].stack == ["expression"]


@pytest.mark.parametrize(
    "in_dict, num_errs",
    [
        (
            {
                "not": [
                    {
                        "or": [
                            {"key": "field", "ista": "val"},
                            {
                                "and": [
                                    {"key": "field", "regex": "^test"},
                                    {"key": "field2", "is": "test1"},
                                ]
                            },
                        ]
                    }
                ]
            },
            1,
        ),
        (
            {
                "not": [
                    {
                        "or": [
                            {"key": "field", "is2": "val"},
                            {
                                "and": [
                                    {"key": "field", "regex2": "^test"},
                                    {"key": "field2", "ista": "test1"},
                                ]
                            },
                        ]
                    }
                ]
            },
            3,
        ),
    ],
)
def test_expression_from_dict_errs_nested(in_dict, num_errs):
    out, errs = Expression.from_dict(in_dict)
    assert out is None
    assert len(errs) == num_errs
    for e in errs:
        assert isinstance(e, ProfileError)
        assert e.component == "expression"
        assert e.stack == ["expression"]
        assert "Could not find implemented" in e.msg


@pytest.mark.parametrize(
    "in_dict, variables, err_msg",
    [
        ({"key": [1, 2], "is": "test"}, {}, "Field must be a string"),
        ({"key": "test", "is": "test"}, {"key": (1, 2)}, "Variable value must be str"),
        ({"key": "test", "is": "test"}, {(1, 2): "val"}, "Variable key must be str"),
    ],
)
def test_expression_from_dict_errs_binary_validate(in_dict, variables, err_msg):
    out, err = Expression.from_dict(in_dict, variables=variables)
    assert out is None
    assert err[0].raw == in_dict
    assert err_msg in err[0].msg


def test_expression_from_dict_errs_unary_validate(mocker):
    in_dict = {
        "and": [
            {"key": "field", "regex": "^test"},
            {"key": "field2", "is": "test1"},
        ]
    }
    validate = mocker.patch(
        "fw_classification.classify.expressions.base.UnaryExpression.validate"
    )
    validate.return_value = ["test_err"]
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].raw == in_dict
    assert err[0].msg == "test_err"


@pytest.mark.parametrize(
    "cls,args,mock",
    [
        (Is, ("test", "value"), "matches"),
        (Set, ("field", "value"), "apply"),
    ],
)
def test_match_evaluate(mocker, cls, args, mock):
    x = cls(*args)
    matches_mock = mocker.patch.object(x, mock)
    i_dict = MagicMock()
    x.evaluate(i_dict)
    matches_mock.assert_called_once_with(i_dict)
