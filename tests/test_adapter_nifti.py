import json
from unittest.mock import MagicMock

import pytest
from dotty_dict import dotty
from flywheel_gear_toolkit.utils.metadata import Metadata
from fw_client import ClientError, FWClient

from fw_classification.adapters import nifti
from fw_classification.adapters.utils import FileInput

# pylint: disable=protected-access


@pytest.fixture
def example_sidecar():
    return {"SeriesDescription": "test", "ProtocolName": "test1"}


@pytest.fixture
def example_nifti_header():
    return {
        "aux_file": "",
        "bitpix": 16,
        "cal_max": 0,
        "cal_min": 0,
        "data_type": "",
        "datatype": 4,
        "db_name": "",
        "descrip": "TE=2.9e+02;Time=144415.395;phase=1",
        "dim": [3, 208, 120, 128, 1, 1, 1, 1],
        "pixdim": [1, 1.440000057220459, 2, 2, 3.200000047683716, 0, 0, 0],
        "qform_code": 1,
        "qoffset_x": -149.03733825683594,
        "qoffset_y": -102.50399780273438,
        "qoffset_z": -154.5133056640625,
        "regular": "r",
        "scl_inter": None,
        "scl_slope": None,
        "session_error": 0,
        "sform_code": 1,
        "sizeof_hdr": 348,
        "slice_code": 0,
        "srow_x": [1.4399871826171875, 0, 0, -149.03733825683594],
        "srow_y": [0, 2, 0, -102.50399780273438],
        "srow_z": [0, 0, 2, -154.5133056640625],
        "toffset": 0,
        "vox_offset": 0,
        "xyzt_units": 10,
    }


def test_nifti_fw_adapter_convert_bids_timings_to_dicom(example_sidecar):
    example_sidecar["RepetitionTime"] = 2
    example_sidecar["EchoTime"] = 2
    example_sidecar["InversionTime"] = 1
    example_sidecar["ExposureTime"] = 2
    example_sidecar["FrameDuration"] = 3
    example_sidecar["UntouchedTimings"] = 3

    expected = example_sidecar.copy()
    expected["RepetitionTime"] = 2000
    expected["EchoTime"] = 2000
    expected["InversionTime"] = 1000
    expected["ExposureTime"] = 2000
    expected["FrameDuration"] = 3000
    expected["UntouchedTimings"] = 3

    out = nifti.convert_bids_timings_to_dicom(example_sidecar)
    assert out == expected


def test_nifti_fw_adapter_init(empty_file, gear_context):
    out = nifti.NiftiFWAdapter(empty_file, gear_context)
    assert out.sidecar == ""


def test_nifti_fw_adapter_preprocess_no_info(
    mocker, empty_file, gear_context, example_sidecar
):
    get_sidecar = mocker.patch("fw_classification.adapters.nifti.get_sidecar")
    get_sidecar.return_value = ("sidecar", example_sidecar, [])
    f = nifti.NiftiFWAdapter(empty_file, gear_context)
    out = f.preprocess()
    i_dict = {"file": FileInput(**empty_file).object.dict()}
    i_dict["file"]["info"] = {"header": {"dicom": example_sidecar}}
    assert out == i_dict
    get_sidecar.assert_called_once_with(f.fw, f.file)


def test_nifti_fw_adapter_preprocess_info_exists(
    mocker, empty_file, gear_context, example_sidecar, example_nifti_header
):
    get_sidecar = mocker.patch("fw_classification.adapters.nifti.get_sidecar")
    empty_file["object"]["info"] = {"header": {"nifti": example_nifti_header}}
    get_sidecar.return_value = ("sidecar", example_sidecar, [])
    f = nifti.NiftiFWAdapter(empty_file, gear_context)
    out = f.preprocess()
    i_dict = {"file": FileInput(**empty_file).object.dict()}
    i_dict["file"]["info"] = {
        "header": {"dicom": example_sidecar, "nifti": example_nifti_header}
    }
    assert out == i_dict
    get_sidecar.assert_called_once_with(f.fw, f.file)


@pytest.mark.parametrize(
    "to_add, in_sidecar, in_file",
    [
        (
            {"tags": ["test1"], "info": {"test": "test1"}},
            {},
            {"tags": ["test1"], "info": {"test": "test1"}},
        ),
        (
            {"classification": {"Intent": "test1"}},
            {"classification": {"Intent": "test1"}},
            {"classification": {"Intent": "test1"}},
        ),
    ],
)
def test_nifti_fw_adapter_postprocess(
    empty_file, gear_context, to_add, in_sidecar, in_file
):
    raw_file = {"file": empty_file["object"]}
    raw_file["file"].update(to_add)
    out = dotty(raw_file)
    in_file = empty_file
    in_file.update({"hierarchy": {"type": "acquisition", "id": "1"}})
    gear_context.metadata = Metadata(MagicMock())
    f = nifti.NiftiFWAdapter(empty_file, gear_context)
    f.sidecar = "test"
    f.b_files = ["test.bval", "test.bvec"]
    res = f.postprocess(True, out)
    assert res
    files_ = gear_context.metadata._metadata["acquisition"]["files"]
    assert len(files_) == 4
    in_sidecar.setdefault("info", {})
    assert {**in_sidecar, "modality": "", "name": "test"} in files_
    assert {**in_sidecar, "modality": "", "name": "test.bval"} in files_
    assert {**in_sidecar, "modality": "", "name": "test.bvec"} in files_


def test_nifti_fw_adapter_postprocess_empty_sidecar(empty_file, gear_context):
    raw_file = {"file": empty_file["object"]}
    raw_file["file"].update({"classification": {"Intent": "test1"}})
    out = dotty(raw_file)
    in_file = empty_file
    in_file.update({"hierarchy": {"type": "acquisition", "id": "1"}})
    gear_context.metadata = Metadata(MagicMock())
    f = nifti.NiftiFWAdapter(empty_file, gear_context)
    f.sidecar = ""
    f.b_files = []
    res = f.postprocess(True, out)
    assert res
    files_ = gear_context.metadata._metadata["acquisition"]["files"]
    assert len(files_) == 1


def test_nifti_fw_adapter_get_sidecar(core, example_sidecar):
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "dicom", "file_id": "1"},
            {"name": "my_file.json", "type": "source code", "file_id": "2"},
            {"name": "my_test_file_imaginary.bvec", "type": "bvec", "file_id": "3"},
            {"name": "my_test_file_imaginary.bve", "type": "bvec", "file_id": "6"},
            {"name": "my_test_file.json", "type": "source code", "file_id": "4"},
            {
                "name": "my_test_file_imaginary.json",
                "type": "source code",
                "file_id": "5",
            },
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)

    def stream_example_sidecar(*_args, **_kwargs):
        return json.dumps(example_sidecar).encode("utf-8"), 200

    core.add_callback(
        "/api/acquisitions/1/files/my_test_file_imaginary.json", stream_example_sidecar
    )
    file = MagicMock()
    file.location.name = "my_test_file_imaginary.nii.gz"
    file.hierarchy.type = "acquisition"
    file.hierarchy.id = "1"
    fw = FWClient(url=core.url, api_key="test")
    name, file_dict, b_files = nifti.get_sidecar(fw, file)
    assert name == "my_test_file_imaginary.json"
    assert file_dict == example_sidecar
    assert b_files == ["my_test_file_imaginary.bvec"]


def test_nifti_fw_adapter_get_sidecar_not_exist(core, example_sidecar, monkeypatch):
    monkeypatch.setenv("NIFTI_RETRY_TIME", "1")
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "dicom", "file_id": "1"},
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)

    def stream_example_sidecar(*_args, **_kwargs):
        return json.dumps(example_sidecar).encode("utf-8"), 200

    core.add_callback("/api/files/4/download", stream_example_sidecar)
    file = MagicMock()
    file.location.name = "my_test_file_imaginary.nii.gz"
    file.hierarchy.type = "acquisition"
    file.hierarchy.id = "1"
    fw = FWClient(url=core.url, api_key="test")
    with pytest.raises(ValueError):
        _ = nifti.get_sidecar(fw, file)


def test_nifti_fw_adapter_get_sidecar_cant_download(core, monkeypatch):
    monkeypatch.setenv("NIFTI_RETRY_TIME", "1")
    acq_files = {
        "files": [
            {"name": "my_file.json", "type": "source code", "file_id": "1"},
        ]
    }
    core.add_response("/api/acquisitions/1", acq_files)

    def stream_example_sidecar(*_args, **_kwargs):
        return b"", 401

    core.add_callback("/api/files/1/download", stream_example_sidecar)
    file = MagicMock()
    file.location.name = "my_file.nii.gz"
    file.hierarchy.type = "api/acquisition"
    file.hierarchy.id = "1"
    fw = FWClient(url=core.url, api_key="test")
    with pytest.raises(ClientError):
        _ = nifti.get_sidecar(fw, file)
