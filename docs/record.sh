#!/usr/bin/env bash
test -z "$TRACE" || set -x
set -eu
USAGE="Usage:
  $0 [COMMAND] [DST]

Record output of the given command.
"

read -ra ORIG_TERMSIZE <<<"$(stty size)"

main() {
    echo "$*" | grep -Eqvw -- "-h|--help|help" || { echo "$USAGE"; exit; }

    log "Recording output"
    trap cleanup INT EXIT
    stty rows 30;
    stty columns 100;
    asciinema rec --overwrite -c "$1" "$2"
}


cleanup() {
    stty rows "${ORIG_TERMSIZE[0]}" columns "${ORIG_TERMSIZE[1]}"
    exit $?
}


# logging and formatting utilities
log() { printf "\e[32mINFO\e[0m %s\n" "$*" >&2; }
err() { printf "\e[31mERRO\e[0m %s\n" "$*" >&2; }
die() { err "$@"; exit 1; }
quiet() { "$@" >/dev/null 2>&1; }


main "$@"
