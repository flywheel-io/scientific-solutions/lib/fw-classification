from fw_classification.classify.utils import ProfileError


def test_profile_error_init():
    err = ProfileError("test1", "my_test", {"test": "test"})
    assert err.component == "profile"
    assert err.c_name == "test1"
    assert err.msg == "my_test"
    assert err.raw == {"test": "test"}
    assert err.stack == ["profile (test1)"]


def test_profile_add_component():
    err = ProfileError("", "", "")
    err.add_component("test")
    assert err.stack == ["profile", "test"]
    err.add_component("test1")
    assert err.stack == ["profile", "test", "test1"]


def test_profile_repr():
    err = ProfileError("test1", "my_test", {"test": "test"})
    err.add_component("test2")
    err.add_component("test2 (test)")
    print(repr(err))
    expected = (
        "profile (test1): my_test\n\tRaw dict: {'test': 'test'}"
        "\n  Traceback:\n\ttest2 (test)\n\ttest2\n\tprofile (test1)"
    )
    assert repr(err) == expected
