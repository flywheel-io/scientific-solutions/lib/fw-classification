<!-- markdownlint-disable code-block-style -->
# Adapters

Adapters are extensions to the base classification engine that make dealing with
particular files easier.

Each adapter implements a `preprocess` and/or `postprocess` method that does some work
before or after classification. For example the [`DICOMAdapter`](#dicom-adapter) uses the
`preprocess` method to pull out the DICOM header.

## Flywheel Adapters

The following adapters are meant to be used in Flywheel via gears.

### FWAdapter

The FW adapter only implements the `preprocess` method.

The `preprocess` phase of FW Adapter pulls the entire hierarchy above
the input file and includes the relevant fields from each container.

These are collected into a dictionary with the following structure, and
passed into `run_classification`

```python
{
  "file": { ... },
  "acquisition": { ... },
  "session": { ... },
  "subject": { ... },
  "project": { ... },
}
```

With this dictionary populated, you can set metadata on the parent
containers or make decisions based on parent containers.

**Example:** Set the subject sex from a DICOM PatientSex

```python
- match:
    - key: file.info.header.dicom.PatientSex:
      is: 'M'
  action:
    - key: subject.sex
      set: 'M'
- match:
    - key: file.info.header.dicom.PatientSex:
      is: 'F'
  action:
    - key: subject.sex
      set: 'F'
```

**Example:** Set custom classification based on a project key

```yaml
- match:
    - key: project.info.cohort
      is: 'Experiment'
  action:
    - key: file.classification.Intent
      set: 'Experimental'
```

### NiftiFWAdapter

The NIfTI adapter extends the [`FWAdapter`](#fwadapter), but in addition to pulling the
hierarchy information, it attempts to find a `json` file with the same stem name in the
same container, this file should be it's sidecar output from dcm2niix containing file
metadata from the original DICOM.

!!! note

    This metadata is populated under `file.info.header.dicom: <sidecar_dict>`. but only
    for the duration of the gear for classification.  Metadata extraction is a separate
    task and not all info from the sidecar should be populated under `file.info.header.dicom`
    since some fields are derived, and thus does not represent the raw dicom header.

!!! note

    When this adapter reads a sidecar file, it automatically converts the
    following fields from BIDS units to DICOM units for consistency with the
    classification profiles.
    ``` 
    EchoTime
    RepetitionTime
    InversionTime
    ExposureTime
    FrameDuration
    ```
    As mentioned above, this is only for the duration of
    the gear for classification and this is not stored anywhere.

The NIfTI adapter adds the the classification to the sidecar json as well, but
as of now, it only copies over the `classification` and `modality` keys onto
the sidecar, this avoids copying over things like `mimetype` and `tags`
which could negatively affect how the file is treated in Flywheel, as well
as info fields such as `qc` which is irrelevant to the sidecar.

## Metadata Adapters

The following adapters find metadata by extracting it from the input file.

### Dicom Adapter

This adapter uses the [fw-file](https://gitlab.com/flywheel-io/tools/lib/fw-file) library
to extract the dicom header from the input file into the `file.info.header.dicom` key:

```python
{
  "file": {
    "info": {
      "header": {
        "dicom": {
          "FileMetaInformationGroupLength": 216,
          # ... Other dicom tags
        }
    }
}
```
