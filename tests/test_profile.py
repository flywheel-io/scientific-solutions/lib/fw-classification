from contextlib import contextmanager
from pathlib import Path

import pytest
from ruamel.yaml import YAML

from fw_classification import Profile


@pytest.fixture
def adjust_example_profile(tmp_path):
    @contextmanager
    def _gen(path, new_name=None):
        y = YAML(typ="safe")
        with open(path, "r", encoding="utf-8") as fp:
            profile_raw = y.load(fp)
        yield (profile_raw, tmp_path / (new_name if new_name else path.name))
        with open(
            tmp_path / (new_name if new_name else path.name), "w", encoding="utf-8"
        ) as fp:
            y.dump(profile_raw, fp)

    return _gen


def test_profile_init(get_profile):
    profile_path = get_profile("optha-v3.yml")
    profile = Profile(profile_path)
    assert profile.name == "OPTHA classifier"
    block_names = [
        "dicom_file",
        "needs_update",
        "opt_classify",
        "cleanup_modality",
        "set_laterality",
    ]
    for (_, block), name in zip(profile.blocks.items(), block_names):
        assert block.name == name
    assert all(
        profile_path.with_suffix("") / block in profile.block_results
        and not profile.block_results[profile_path.with_suffix("") / block]
        for block in block_names
    )


def test_profile_file_not_exist(tmp_path):
    profile_path = tmp_path / "profile.yaml"
    with pytest.raises(ValueError):
        _ = Profile(profile_path)


def test_profile_file_invalid_yaml(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    with adjust_example_profile(profile_path) as (_, path):
        pass
    with open(path, "ab") as fp:
        fp.write(b"Cannot be read")
    with pytest.raises(ValueError):
        _ = Profile(path)


def test_profile_include_not_exist(get_profile, adjust_example_profile):
    include = ["test-profile.yml"]
    profile_path = get_profile("optha-v3.yml")
    with adjust_example_profile(profile_path) as (prof, path):
        prof["includes"] = include

    with pytest.raises(RuntimeError):
        _ = Profile(path)


def test_profile_name_not_defined(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    with adjust_example_profile(profile_path) as (raw, path):
        raw["name"] = None
    with pytest.raises(SystemExit):
        _ = Profile(path)


def test_resolve_include(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets"
    include = "test.yml"
    with adjust_example_profile(profile_path) as (prof, path):
        prof["includes"] = [include]
    # Include present
    my_profile = Profile(path, include_search_dirs=[include_path])
    assert list(my_profile.blocks.items())[0][1].name == "test_rule"
    # Include not present
    my_profile2 = Profile(profile_path)
    assert "test_rule" not in [block.name for _, block in my_profile2.blocks.items()]


@pytest.mark.parametrize(
    "eval,exp",
    [
        (
            # second_A/test_rule evaluates to true, so first/bla should be run
            {"ProtocolName": "FA"},
            {
                "ProtocolName": "FA",
                "file": {"modality": "FA", "classification": {"Type": ["FA"]}},
            },
        ),
        (
            # second_A/test_rule evaluates to false so first/bla should not be run
            # but second_B/test_rule does evaluate
            {"ProtocolName": "FB"},
            {"ProtocolName": "FB", "file": {"modality": "FB"}},
        ),
    ],
)
def test_resolve_double_nested_include(caplog, get_profile, eval, exp):
    caplog.set_level(0)
    profile_path = get_profile("main.yml")
    profile = Profile(profile_path)
    assert len(profile.blocks) == 3
    assert all(
        k in profile.include_map.keys() for k in ["first", "second_A", "second_B"]
    )
    # assert duplicate block name is fine and does not collide
    assert len(caplog.record_tuples) == 0
    assert len([b for b in profile.blocks if b.name == "test_rule"]) == 2
    # assert can evaluate (and resolve nested depends_on)
    _, out = profile.evaluate(eval)
    assert exp == out


def test_resolve_include_duplicate_profile_name(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets"
    include = "test.yml"
    with adjust_example_profile(profile_path) as (prof, path):
        prof["includes"] = [include, include]
    # Include present
    with pytest.raises(SystemExit):
        _ = Profile(path, include_search_dirs=[include_path])


def test_resolve_include_err(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets"
    include = "test.yml"
    with adjust_example_profile(profile_path) as (prof, main_path):
        prof["includes"] = [include]
    with adjust_example_profile(include_path / "test.yml") as (
        include_prof,
        secondary_path,
    ):
        include_prof["name"] = None
    # Include present
    with pytest.raises(SystemExit):
        _ = Profile(main_path, include_search_dirs=[secondary_path.parents[0]])


def test_depends_on_with_scoped_name(get_profile):
    # Include
    profile_path = get_profile("test_use_include.yml")
    profile = Profile(profile_path)
    i_dict = {}
    res, out = profile.evaluate(i_dict)
    assert out["result"] is True


@pytest.mark.xfail(reason="Namespaced blocks deprecate old duplicate block detection.")
def test_resolve_include_duplicate_block_local(
    get_profile, adjust_example_profile, caplog
):
    caplog.set_level(0)
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets/test.yml"
    include = "test.yml"
    dup_rule = {}
    with adjust_example_profile(profile_path) as (prof, main_path):
        prof["includes"] = [include]
        dup_rule = prof["profile"][0]
    with adjust_example_profile(include_path) as (include_prof, secondary_path):
        include_prof["profile"].append(dup_rule)
    # Include present
    p = Profile(main_path, include_search_dirs=[secondary_path.parents[0]])
    assert "Found duplicate block" in caplog.record_tuples[0][2]
    assert p.block_map["dicom_file"] == "local"


@pytest.mark.xfail(reason="Namespaced blocks deprecate old duplicate block detection.")
def test_resolve_include_duplicate_block_included(
    get_profile, adjust_example_profile, caplog
):
    caplog.set_level(0)
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets"
    include1 = "test.yml"
    include2 = "test2.yml"
    with adjust_example_profile(profile_path) as (prof, main_path):
        prof["includes"] = [include1, include2]
    # Change second include name so as not to hit that error, hit duplicate
    # block error instead
    with adjust_example_profile(include_path / "test.yml", include2) as (
        include_prof,
        secondary_path,
    ):
        include_prof["name"] = "other_name_not_duplicate"
        include_prof["profile"][0]["rules"][0]["action"][0]["set"] = "test"
    # Include present
    p = Profile(
        main_path, include_search_dirs=[include_path, secondary_path.parents[0]]
    )
    assert "Found duplicate block" in caplog.record_tuples[0][2]
    assert p.block_map["test_rule"] == "other_name_not_duplicate"
    assert p.blocks["test_rule"].rules[0].action.exprs[0].value == "test"


def test_duplicate_block(get_profile, caplog):
    caplog.set_level(0)
    profile_path = get_profile("test_dups.yml")
    p = Profile(profile_path)
    assert "Found duplicate block" in caplog.record_tuples[0][2]
    assert p.block_map[profile_path.with_suffix("") / "test_rule"] == "local"


def test_profile_evaluate(get_profile):
    profile_path = get_profile("optha-v3.yml")
    profile = Profile(profile_path)
    i_dict = {
        "file": {
            "type": "dicom",
            "info": {
                "header": {"dicom": {"ProtocolName": "FA", "ImageLaterality": "R"}}
            },
        },
        "acquisition": {"label": "OCT"},
    }
    res, out = profile.evaluate(i_dict)
    assert res
    assert out["file.classification"] == {
        "Type": ["Fluorescein Angiography"],
        "Sub-Type": ["Standard Field"],
        "Laterality": "RIGHT",
    }
    assert out["file.modality"] == "OCT"


def test_profile_block_depends_unknown(get_profile, adjust_example_profile):
    profile_path = get_profile("optha-v3.yml")
    with adjust_example_profile(profile_path) as (prof, path):
        prof["profile"][2]["depends_on"] = ["random_block"]

    profile = Profile(path)
    with pytest.raises(ValueError) as e:
        _ = profile.evaluate({})
    assert "random_block" in e.value.args[0]


def test_profile_repr(get_profile):
    profile_path = get_profile("optha-v3.yml")
    profile = Profile(profile_path)
    assert str(profile).startswith("Profile: OPTHA classifier\n\nBlocks:\n")


def test_profile_rule_error(get_profile, adjust_example_profile, mocker):
    profile_path = get_profile("optha-v3.yml")
    include_path = Path(__file__).parents[0] / "assets/test.yml"
    raw_val = [{"key": "$fihd.ProtocolName", "ista": "FA"}]
    with adjust_example_profile(profile_path) as (prof, path):
        prof["profile"][2]["rules"][0]["match"] = raw_val
        prof["profile"][3]["rules"][0]["match"][0] = raw_val[0]
    g_profile = mocker.patch("fw_classification.classify.includes.get_profile")
    g_profile.return_value = include_path
    # Include present
    with pytest.raises(SystemExit):
        _ = Profile(path)


def test_profile_dumps(get_profile, tmp_path):
    p = Profile(get_profile("optha-v3.yml"))
    p.to_yaml(tmp_path / "tmp.yaml")
    p2 = Profile(tmp_path / "tmp.yaml")
    assert repr(p) == repr(p2)
