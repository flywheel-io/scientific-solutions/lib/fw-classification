import pytest
from dotty_dict import dotty

from fw_classification.classify import block
from fw_classification.classify.block import Evaluate
from fw_classification.classify.expressions import Is
from fw_classification.classify.rule import Match, Rule


@pytest.fixture
def example_block():
    block = {
        "name": "opt_classify",
        "description": "Set FP/OCT modality and classifications based on ProtocolName",
        "variables": {
            "fihd": "file.info.header.dicom",
            "fm": "file.modality",
            "fc": "file.classification",
        },
        "evaluate": "first",
        "depends_on": ["needs_update"],
        "rules": [
            {
                "match_type": "all",
                "match": [{"key": "$fihd.ProtocolName", "is": "FA"}],
                "action": [
                    {"key": "$fm", "set": "FP"},
                    {
                        "key": "$fc.Type",
                        "set": ["Fluorescein Angiography"],
                    },
                    {"key": "file.classification.Sub-Type", "set": ["Standard Field"]},
                ],
            },
            {
                "match_type": "all",
                "match": [{"key": ".ProtocolName", "is": "FA-4W Sweep"}],
                "action": [
                    {"key": "file.modality", "set": "FP"},
                    {
                        "key": "$fc.Type",
                        "set": ["Fluorescein Angiography"],
                    },
                    {"key": "$fc.Sub-Type", "set": ["Wide Field"]},
                ],
            },
            None,
        ],
    }
    return block


def test_block_nested_variables(example_block):
    block_raw = example_block
    block_raw["rules"] = [
        {
            "match_type": "all",
            "match": [
                {
                    "or": [
                        {"key": "$fihd.test", "is": "val"},
                        {
                            "and": [
                                {"key": "$fihd.test2", "regex": "^test"},
                                {"key": "field2", "is": "test1"},
                            ]
                        },
                    ]
                }
            ],
        }
    ]
    my_block, err = block.Block.from_dict(block_raw)
    assert my_block
    assert not err
    assert my_block.evaluate(
        dotty(
            {
                "file": {
                    "info": {"header": {"dicom": {"test": "val", "test2": "test"}}}
                },
                "field2": "test1",
            }
        )
    )


@pytest.mark.parametrize("depends, dep_out", [(None, []), (["test1"], ["test1"])])
def test_block_init(depends, dep_out):
    rules = [Rule(Match([Is("test", "test1")]), None)]
    variables = {"fc": "file.classification"}
    my_block = block.Block("test_block", rules, depends_on=depends, variables=variables)
    assert my_block.name == "test_block"
    assert my_block.rules == rules
    assert my_block.eval_type == Evaluate.First
    assert my_block.depends_on == dep_out
    assert my_block.variables == [variables]


def test_block_repr(example_block):
    my_block, err = block.Block.from_dict(example_block)
    assert not err

    expected = (
        "\nIf all of (needs_update) executed, then execute the first match of the "
        + "following:\n\n\t-------------------- Rule 0 --------------------\n\tMatch"
        + " if All are True: \n\t\t- $fihd.ProtocolName is FA\n\t\n"
        + "\tDo the following: \n\t\t- set $fm to FP\n\t\t- set "
        + "$fc.Type to ['Fluorescein Angiography']\n\t\t- set "
        "file.classification.Sub-Type to ['Standard Field']\n\n\n\t"
        + "-------------------- Rule 1 --------------------\n\tMatch if All are True:"
        + " \n\t\t- .ProtocolName is FA-4W Sweep\n\t\n\tDo the following: \n\t\t- set"
        + " file.modality to FP\n\t\t- set $fc.Type to "
        + "['Fluorescein Angiography']\n\t\t- set $fc.Sub-Type to ['Wide Field']\n\n\n"
    )
    assert repr(my_block) == expected
    # Remove `None` from example_block for assertion
    example_block["rules"] = example_block["rules"][:-1]
    assert my_block.to_dict() == example_block


def test_evaluate_block_from_dict(example_block):
    i_dict = dotty({})
    i_dict["file.info.header.dicom"] = {"ProtocolName": "FA"}
    my_block, err = block.Block.from_dict(example_block)
    assert not err
    assert my_block.evaluate(i_dict)
    assert i_dict["file.info.header.dicom.ProtocolName"] == "FA"
    assert i_dict["file.classification.Type"] == ["Fluorescein Angiography"]
    assert i_dict["file.classification.Sub-Type"] == ["Standard Field"]
    assert i_dict["file.modality"] == "FP"


def test_evaluate_all_block_from_dict(example_block):
    example_block["evaluate"] = "all"
    example_block["rules"][1] = {
        "match_type": "any",
        "match": [
            {"key": "$fihd.ImageLaterality", "is": "R"},
            {"key": "acquisition.label", "regex": "(^|[^a-zA-Z])(R|RE)([^a-zA-Z]|$)"},
            {"key": "acquisition.label", "regex": "(^|[^a-zA-Z])(OD)([^a-zA-Z]|$)"},
            {"key": "acquisition.label", "regex": "RIGHT"},
        ],
        "action": [{"key": "$fc.Laterality", "set": ["RIGHT"]}],
    }

    i_dict = dotty({})
    i_dict["file.info.header.dicom"] = {"ProtocolName": "FA", "ImageLaterality": "R"}
    my_block, err = block.Block.from_dict(example_block)
    assert not err
    assert my_block.eval_type == Evaluate.All
    assert my_block.evaluate(i_dict)
    assert i_dict["file.info.header.dicom.ProtocolName"] == "FA"
    assert i_dict["file.classification.Type"] == ["Fluorescein Angiography"]
    assert i_dict["file.classification.Sub-Type"] == ["Standard Field"]
    assert i_dict["file.modality"] == "FP"
    assert i_dict["file.classification.Laterality"] == ["RIGHT"]


def test_block_from_dict_raises_no_rules(example_block):
    i_dict = example_block
    i_dict["rules"] = []
    out, err = block.Block.from_dict(i_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "block"
    assert err[0].c_name == "opt_classify"
    assert err[0].msg == "At least one rule must be specified."
    assert err[0].stack == ["block (opt_classify)"]


def test_block_from_dict_raises_no_block_name(example_block):
    i_dict = example_block
    i_dict["name"] = ""
    out, err = block.Block.from_dict(i_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "block"
    assert err[0].msg == "Name must be specified."
    assert err[0].stack == ["block"]


def test_block_from_dict_raises_unknown_eval_type(example_block):
    i_dict = example_block
    i_dict["evaluate"] = "my_test"
    out, err = block.Block.from_dict(i_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "block"
    assert err[0].c_name == "opt_classify"
    assert err[0].msg == "Unknown evaluate string: my_test"
    assert err[0].stack == ["block (opt_classify)"]


def test_block_from_dict_propagates_match_rule_error(example_block):
    i_dict = example_block
    raw_val = [{"key": "$fihd.ProtocolName", "ista": "FA"}]
    i_dict["rules"][0]["match"] = raw_val
    out, err = block.Block.from_dict(i_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "expression"
    assert err[0].raw == raw_val[0]
    assert "Could not find implemented" in err[0].msg
    assert err[0].stack == ["expression", "match", "rule", "block (opt_classify)"]


def test_block_from_dict_propagates_action_rule_error(example_block):
    i_dict = example_block
    raw_val = {"key": "$fm", "set2": "FP"}
    i_dict["rules"][0]["action"][0] = raw_val
    out, err = block.Block.from_dict(i_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "expression"
    assert err[0].raw == raw_val
    assert "Could not find implemented" in err[0].msg
    assert err[0].stack == ["expression", "action", "rule", "block (opt_classify)"]


def test_block_multi_variables_valid(example_block):
    example_block["variables"]["fihd"] = {0: "file.info.header.dicom", 1: "test_key"}
    b, err = block.Block.from_dict(example_block)
    assert not err
    assert len(b.variables) == 2
    assert b.variables[0] == {
        "fihd": "file.info.header.dicom",
        "fm": "file.modality",
        "fc": "file.classification",
    }
    assert b.variables[1] == {
        "fihd": "test_key",
        "fm": "file.modality",
        "fc": "file.classification",
    }

    example_block["variables"]["fihd"] = ["file.info.header.dicom", "test_key"]
    b, err = block.Block.from_dict(example_block)
    assert not err
    assert len(b.variables) == 2
    assert b.variables[0] == {
        "fihd": "file.info.header.dicom",
        "fm": "file.modality",
        "fc": "file.classification",
    }
    assert b.variables[1] == {
        "fihd": "test_key",
        "fm": "file.modality",
        "fc": "file.classification",
    }


def test_block_multi_variables_invalid(example_block):
    example_block["variables"]["fihd"] = {0: "file.info.header.dicom", 2: "test_key"}
    b, err = block.Block.from_dict(example_block)
    assert len(err) == 1
    assert b is None
    assert "Variable index 2 is invalid" in err[0].msg

    example_block["variables"]["fihd"] = {1: "file.info.header.dicom", 2: "test_key"}
    b, err = block.Block.from_dict(example_block)
    assert len(err) == 1
    assert b is None
    assert "Variable index 1 is invalid" in err[0].msg

    example_block["variables"] = ["file.info.header.dicom", "test_key"]
    b, err = block.Block.from_dict(example_block)
    assert len(err) == 1
    assert b is None
    assert "Variables must be a mapping" in err[0].msg
