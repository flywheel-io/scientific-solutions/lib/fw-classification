<!-- markdownlint-disable no-duplicate-heading -->
# Release Notes

## 0.4.8

### Enhancement

* Add `is_empty` boolean operator to check whether a field exists but is empty
(`None`, `[]`, `""`)

## 0.4.7

### Bug Fix

* Fix API call to get NIfTI sidecar files

## 0.4.6

### Maintenance

* Update dependencies and CI
* Update `DICOM` class arguments

### Bug Fix

* Fix NIfTI postprocessing to not add metadata to an empty-string "file"
when sidecar does not exist

## 0.4.5

### Maintenance

* Replace `fw_core_client` with `fw_client`
* Import `get_dicom_header` from `fw_gear_file_metadata_importer` instead of
`flywheel_gear_toolkit`

## 0.4.4

### Enhancement

* Convert timings for sidecar files from BIDS units to DICOM units

## 0.4.3

### Bug Fix

* Fix validation for resolve functionality.

## 0.4.2

### Enhancement

* Enable resolve "value" functionality for binary expressions.

## 0.4.1

### Fixes

* Fix the handling of multi level include profiles

## 0.4.0

### Enhancements

* Allow for profile selection in block dependencies

## 0.3.5

## 0.3.4

### Enhancements

* Update to new metadata methods

## 0.3.3

### Bug fix

* Allow spaces in variable syntax

## 0.3.2

### Enhancements

* Add `is_numeric` operator

### Maintenance

* Change warning on non-numeric values to `log.warning`
* Change docs to mkdocs

### Bug fix

* Propagate variables to sub-expressions when using grouping unary operators.

## 0.3.1

### Bug fix

* Handle numeric values which are set to None in input dictionary.

## 0.3.0

### Enhancements

* Add numeric comparison operators
* Add filename as a key in input dictionary

### Maintenance

* Update CI and fix linting

## 0.2.3

### Enhancements

* Add classification of NIfTIs to BVAL/BVEC files when applicable.

### Bug

* Fix install issue with python3.8/3.9 compatibility due to `zoneinfo`

## 0.2.2

### Maintenance

* Use temporary directory for git dependencies.

### Documentation

* Update docs for implemented expressions and include configuration values.

## 0.2.1

### Enhancements

* Make :class:`fw_classification.classify.expressions.Add` not duplicate values by default.

## 0.2.0

### Maintenance

* Move classification profiles to a separate repo at
`classification-profiles <https://gitlab.com/flywheel-io/public/classification-profiles>`_

### Enhancements

* Allow resolving includes as relative/absolute paths, and git urls,
  see :ref:`includes` for details.

## 0.1.6

### Bug

* Add modality to sidecard in :ref:`NiftiFWAdapter`

## 0.1.5

### Maintenance

* Correctly print sidecar in :ref:`NiftiFWAdapter`

## 0.1.4

### Maintenance

* Only add classification to sidecar json.

## 0.1.3

No code change

### Documentation

* Revert to README.md for project description

## 0.1.2

### Bugfix

* Default to `MatchType.Any` when `match_type` not set in
`fw_classification.classify.rule.Rule.from_dict`

### Maintenance

* Add README with usage in markdown on repo
* Adjust `readme` directive to render getting started on PyPI
<!-- markdownlint-enable no-duplicate-heading -->
