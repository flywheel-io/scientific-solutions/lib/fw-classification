from pathlib import Path
from unittest.mock import MagicMock

import pytest

PROFILE_PATH = Path(__file__).parents[0] / "assets"

pytest_plugins = ("fw_http_testserver",)


@pytest.fixture
def core(http_testserver):
    return http_testserver


@pytest.fixture
def get_profile():
    def inner(profile):
        return PROFILE_PATH / profile

    return inner


@pytest.fixture
def empty_file():
    return {
        "hierarchy": {"id": "", "type": ""},
        "object": {
            "mimetype": "",
            "version": 0,
            "type": "",
            "modality": "",
            "size": 0,
            "zip_member_count": 0,
            "file_id": "id",
        },
        "location": {"path": "", "name": ""},
    }


@pytest.fixture
def gear_context():
    gear_context = MagicMock()
    gear_context.config_json = {"inputs": {"api-key": {"key": "test.flywheel.io:test"}}}
    return gear_context
