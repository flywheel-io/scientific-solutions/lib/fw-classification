# Classification Toolkit

The main documentation for the classification-toolkit can be found at the
[fw-classification
Documentation](https://flywheel-io.gitlab.io/scientific-solutions/lib/fw-classification/)

Classification toolkit is a library that can act on arbitrary metadata in the
form of JSON, with a declarative set of rules.  The main use case for this
library is for classification of medical imaging files based on their
metadata, for example, the `SeriesDescription` tag in a DICOM file.

## Install

`poetry add fw-classification`, or with optional dependencies
`poetry add fw-classification -E all`.

## Run classification via cli

<!-- markdownlint-disable line-length -->
```bash
~/ ➜  classify run ./fw_classification/profiles/main.yml ~/Downloads/example.dicom.zip -a dicom
fw_classification:      Block mr_file result: True
fw_classification:      Block set_modality result: True
fw_classification:      Block set_classification_from_acquisition_label_or_series_description result: True
fw_classification:      Block set_contrast_from_acquisition_label_or_series_description result: False
fw_classification:      Block add_features_from_acquisition_label_or_series_description result: False
fw_classification:      Block add_measurement_from_acquisition_label_or_series_description result: False
fw_classification:      Block add_intent_from_acquisition_label_or_series_description result: False
{
  "file": {
    "info": {
      "header": {
        "dicom": {
          "FileMetaInformationGroupLength": 216,
          "WindowWidth": 199.0,
          ... # Many more tags
          "dBdt": 0.0
        }
      }
    },
    "type": "dicom",
    "modality": "MR",
    "classification": {
      "Intent": [
        "Structural"
      ],
      "Measurement": [
        "T1"
      ]
    }
  }
}
```
<!-- markdownlint-enable line-length -->

## Run via python

```python
from fw_classification.classify import run_classification
import json

input_data = {}
with open('/path/to/input','r') as fp:
   input_data = json.load(fp)

result, out_data = run_classification('MR_classifier', input_data)
```

Out:

```bash
~/ ➜   python test.py
{
  "file": {
    "info": {
      "header": {
        "dicom": {
          "FileMetaInformationGroupLength": 216,
          ... # Many more tags
          "WindowWidth": 199.0,
          "dBdt": 0.0
        }
      }
    },
    "type": "dicom",
    "modality": "MR",
    "classification": {
      "Intent": [
        "Structural"
      ],
      "Measurement": [
        "T1"
      ]
    }
  }
}
```

## Development

Install the project using poetry (requires poetry > 1.2.0b1), enable plugin and
pre-commit:

```bash
poetry install --extras "all"
pre-commit install
```
