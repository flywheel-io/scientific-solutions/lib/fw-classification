<!-- markdownlint-disable code-block-style -->
# Profiles

The `profile` describes the rules that will be applied to incoming data.  A `profile`
has a [`name`](#name), [`description`](#description), zero or more
[`includes`](#includes), and a list of [`block`](#block) objects that contain one or
more [`rule`](#rule).  Rules apply their [`action`](#action) section when their
[`match`](#match) section evaluates to `True`.

An example can be found in the repository, or at the bottom of the page
[Example](#example).

## Top level profile Properties

### `name`

Simple name for the profile

**Example:**

```yaml
name: MR-neuro-1
```

### `description`

Description of the profile.

**Example:**

```yaml
name: MR-neuro-1
description: |
  A profile to classify neurological MRI images.
  Should only be used with...
```

### `includes`

List of other templates to include and what order to include them in. The first listed
templates will be included first. These templates can reference:

* A relative path
* An absolute path
* A git url with the format `<git_url>$<path/to/file>`

!!! warning

    In order to use git urls, you must have git installed on your system.
    Otherwise you will get a ``RuntimeError``

**Example:**

```yaml
includes:
  # Relative paths in the same folder as profile definition.
  - MR_classifier.yml
  - CT_classifier.yml
  
  # Absolute paths
  - /Users/naterichman/Documents/classification-profiles/custom-profile.yml

  # Git urls
  - https://gitlab.com/flywheel/io/public/classification-profiles.git$profiles/MR_classifier.yml
```

### `profile`

The bulk of the profile definition begins under the `profile` key, under this key
there are a list of [`block`](#block).

**Example:**

```yaml
name: MR-neuro-1
description: |
  A profile to classify neurological MRI images.
  Should only be used with...
include:
  # Built in templates
  - MR_classifier.yml
  - CT_classifier.yml
profile:
  - name: <block1>
  # ... More blocks
```

## block

A block defines a set of rules and actions to update file metadata

For example the following block attempts to match Siemens 3D T1 and set corresponding
classification with one rule:

```yaml
- name: DICOM_MR_Siemens_3DT1
  variables:
    fihd: file.info.header.dicom
    fc: file.classification
  rules:
    - match_type: 'all'
      match:
        - key: $fihd.Manufacturer
          in: [ 'SIEMENS', 'Siemens' ]
        - key: $fihd.SequenceName
          regex: '^\*tfl3d1.+|^tfl3d1.+'
        - key: $fihd.MRAcquisitionType
          is: '3D'
      action:
        - key: $fc.Measurement
          set: [ 'T1' ]
        - key: $fc.Intent
          set: [ 'Structural' ]
        - key: $fc.Features
          set: [ '3D' ]
```

This block could be written as:

> If the dicom tag `Manufacturer` is either `SIEMENS` or `Siemens` AND
> the dicom tag `SequenceNumber` contains the string `tfl3d1` AND
> the dicom tag `MRAcquisitionType` is `3D` THEN
> Set the classification `Measurement` to `T1`, `Intent` to `Structural` and
> `Features` to `3D`

Blocks have a number of specific keys that are defined below.

### Block `name`

The name of the block.

### Block `evaluate`

The `evaluate` keyword describes how rules in the group will be evaluated, possible
choices are:

* `first`: The first rule to match will be evalauted and no others will be
* `all`: Evaluate all rules that match regardless of whether any other rules matched.

!!! note "Order"
    When you specify `evaluate: fist` order will matter!

### `variables`

`variables` is a dictionary of variables that will be replaced in expression keys.
Variables are accessed by using the dollar sign ($) operator.  For example the following
alias block could be accessed later in a rule:

```yaml
variables:
  fihd: file.info.header.dicom
  fc: file.classification
...
    - match_type: any
      match:
        - key: $fihd.PatientName
          is: Anonymized
      action:
        - key: $fc.Anonymized
          append: PatientName
```

### Looping

Variables can be used to "loop" between values, if multiple values are specified for a variable,
all rules will be evaluated with each value, e.g.

```yaml
variables:
  # Will loop values of label at 0, then 1...
  label:
    0: acquisition.label
    1: file.info.header.dicom.SeriesDescription
  fc: file.classification
...
    - match_type: any
      match:
        - key: $label.PatientName
          contains: Anonymized
      action:
        - key: $fc.Anonymized
          append: PatientName
```

The above block will loop through each value of label, checking if either
`acquisition.label` or `file.info.header.dicom.SeriesDescription` contains `Anonymized`.

### `depends_on`

The `depends_on` keyword specifies a list of previous blocks that matched and executed
successfully on the input JSON.

**Example:**

```yaml
## Block for dicom file:
- name: dicom_file
  evaluate: "first"
  rules:
    - match:
      - key: "file.type"
        is: "dicom"

## Only evaluate when its a dicom file:
- name: dicom_specific_block
  evaluate: "first"
  depends_on:
    - dicom_file
  # ...rest of block definition
```

The second rule would only be evaluated if the first rule returns `True`

#### Using blocks from other files as dependencies

You can select a block from a separate profile that is included (see
[includes](#includes)) by prepending the profile name.
For example, to select the dicom_file block in the MR-neuro-1 profile:

```yaml
name: MR-neuro-2
description: |
  A profile to demonstrate cross-file depends
profile:
  - name: dicom_specific_block
    evaluate: "first"
    depends_on:
      - MR-neuro-1/dicom_file
    # ...rest of block definition
```

## rule

A rule has two parts, the [`match`](#match) section, and the [`action`](#action)
section. The [`action`](#action) section will only be evaluated if the [`match`](#match)
returns `True`.

### `match`

The match section contains a list of `expressions` that specify what fields in the input
metadata should be in order to apply the `action` section of the rule.

Each rule has one configuration option, `match_type` which can be one of:

* `all`: Only match if all expressions evaluate to `True`
* `any`: Match if any expression evaluates to `True`

`expressions` use the following syntax:

```yaml
...
- <key>: <dotty-path-from-file-or-container>
  <operator>: <value>
```

Where `<key>` is a dotty key in a nested dictionary, see the [`keys`](#keys) section for
more details.

The following operators are defined and specify the value that can be supplied.

{{simple_binary_match}}

Expressions can be joined with logical operators.

Logical operators that can be used are:

{{simple_unary_match}}

### `action`

This section specifies what to do to the metadata if the :ref:`match` section evaluates to
True. The `action` section is a list that either `sets`, `updates` or `appends` to
metadata.

Operators:

{{simple_binary_action}}

### keys

Keys follow a dotty notation.  For example, an in JSON document of the form:

```python
{
  "file": {
    "info": {
      "header": {
        "dicom": {
          "ImagePositionPatient": [0.1, 0.2, 0.3],
          "ContributingEquipmentSequence": [
            {
              "Manufacturer": "Flywheel",
              "ManufacturerModelName": "fw_gear_splitter"
            },
            {
              "Manufacturer": "Flywheel",
              "ManufacturerModelName": "fw_cli"
            },
          ]
        }
      }
    }
  }
}
```

Would be addressed in the following way:

```py
    >>> dat = json.loads(<json_doc>)
    >>> dat['file.info.header.dicom.ImagePositionPatient']
    [0.1, 0.2. 0.3]
    >>> dat['file.info.header.dicom.ContributingEquipmentSequence.0.Manufacturer']
    "Flywheel"
    >>> dat['file.info.header.dicom.ContributingEquipmentSequence.$.ManufacturerModelName']
    ['fw_gear_splitter','fw_cli']
```

## Example

```yaml
---
name: OPTHA classifier
profile:

  - name: dicom_file
    evaluate: "first"
    rules:
      - match_type: 'all'
        match:
          - key: file.type
            is: dicom

  - name: needs_update
    depends_on:
      - dicom_file
    variables:
      fihd: file.info.header.dicom
    evaluate: "first"
    rules:
      - match_type: 'any'
        match:
          - key: $fihd.Columns
            exists: true
          - key: acquisition.label
            startswith: 'OCT'
          - key: acquisition.label
            regex: '^OP?T?_'

  - name: opt_classify
    description: |
      Set FP/OCT modality and classifications based on ProtocolName
    evaluate: "first"
    variables:
      fihd: file.info.header.dicom
      fc: file.classification
    depends_on:
      - needs_update

    rules:

      - match_type: "all"
        match:
          - key: $fihd.ProtocolName
            is: 'FA'
        action:
          - key: file.modality
            set: 'FP'
          - key: $fc.Type
            set: ['Fluorescein Angiography']
          - key: $fc.Sub-Type
            set: ['Standard Field']

      - match_type: "all"
        match:
          - key: $fihd.ProtocolName
            is: 'FA-4W Sweep'
        action:
          - key: file.modality
            set: 'FP'
          - key: $fc.Type
            set: ['Fluorescein Angiography']
          - key: $fc.Sub-Type
            set: ['Wide Field']

      # ... other rules

  - name: cleanup_modality
    description: |
      Set modality if not set based on StudyDescription and
      AcquisitionDevideTypeCodeSequence CodeValue
    depends_on:
      - needs_update
    variables:
      fihd: file.info.header.dicom
      fc: file.classification
    evaluate: "first"

    rules:

      - match_type: "any"
        match:
          - key: $fihd.AcquisitionDeviceTypeCodeSequence.$.CodeValue
            is: "A-00FBE"
          - key: acquisition.label
            regex: 'OCT'
          - key: acquisition.label
            regex: '^OP?T?_'
        action:
          - key: file.modality
            set: 'OCT'

      - match_type: "all"
        match:
          - key: $fihd.AcquisitionDeviceTypeCodeSequence.$.CodeValue
            exists: true
          - key: $fihd.StudyDescription
            is: 'CF'
        action:
          - key: file.modality
            set: 'FP'
          - key: $fc.Type
            set: 'Color'

  - name: set_laterality
    description: |
      Determine laterality from ImageLaterality or acquisition label
    depends_on:
      - needs_update
    evaluate: "first"
    aliases:
      fihd: file.info.header.dicom
      fc: file.classification

    rules:

      - match_type: "any"
        match:
          - key: $fihd.ImageLaterality
            in: ['R', 'OD']
          - key: acquisition.label
            regex: '(^|[^a-zA-Z])(R|RE)([^a-zA-Z]|$)'
          - key: acquisition.label
            regex: '(^|[^a-zA-Z])(OD)([^a-zA-Z]|$)'
          - key: acquisition.label
            regex: 'RIGHT'
        action:
          - key: $fc.Laterality
            set: 'RIGHT'

      - match_type: "any"
        match:
          - key: acquisition.label
            regex: '(^|[^a-zA-Z])(L|LE)([^a-zA-Z]|$)'
          - key: acquisition.label
            regex: '(^|[^a-zA-Z])(OS)([^a-zA-Z]|$)'
          - key: acquisition.label
            regex: 'LEFT'
          - key: $fihd.ImageLaterality
            in: ['L', 'OS']
        action:
          - key: $fc.Laterality
            set: 'LEFT'
```
